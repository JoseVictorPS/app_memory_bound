/* 
 * File:   main.c
 * Author: rkendy
 *
 * Created on 5 de Junho de 2013, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#define VALOR_QUALQUER 10

/*
void calcula(float* elementoA, float* elementoB) {
    long long tmp = rand()%100 + 1;
    *elementoA = tmp;
    *elementoB = *elementoA * VALOR_QUALQUER + pow(tmp, tmp);    
}
*/

void job01(int qtdBlocos, long long tamBloco, int qtdLoop, float *vetor) {

    long long i, j, k, tmp;

    for (i = 0; i < qtdBlocos; i++) {
        long long index = i * tamBloco;
        printf("Calculando (%d vezes) Bloco %lld com index %lld ao %lld...\n", qtdLoop, i, index, index + tamBloco - 1);
        for (k = 0; k < qtdLoop; k++) {
            for (j = 1; j < tamBloco; j++) {
                //calcula(&vetor[index+j-1], &vetor[index+j]);
                tmp = rand() % 100 + 1;
                vetor[index + j - 1] = tmp;
                vetor[index + j] = vetor[index + j - 1] * VALOR_QUALQUER + pow(tmp, 2);
            }
        }
    }
}


void job03(int qtdBlocos, long long tamVetor, long long tamBloco, int qtdLoop, float *vetor) {

    long long i, j, k, tmp;
    qtdLoop--;

    printf("Calculando (1 vez) vetor inteiro de index 0 ao %lld...\n", tamVetor - 1);
    for (i = 1; i < tamVetor; i++) {
        tmp = rand() % 100 + 1;
        vetor[i - 1] = tmp;
        vetor[i] = vetor[i - 1] * VALOR_QUALQUER + pow(tmp, 2);
        //calcula(&vetor[i-1], &vetor[i]);
    }

    for (i = 0; i < qtdBlocos; i++) {
        long long index = i * tamBloco;
        printf("Calculando (%d vezes) Bloco %lld com index %lld ao %lld...\n", qtdLoop, i, index, index + tamBloco - 1);
        for (k = 0; k < qtdLoop; k++) {
            for (j = 1; j < tamBloco; j++) {
                tmp = rand() % 100 + 1;
                vetor[index + j - 1] = tmp;
                vetor[index + j] = vetor[index + j - 1] * VALOR_QUALQUER + pow(tmp, tmp);
                //calcula(&vetor[index+j-1], &vetor[index+j]);
            }
        }
    }
}

void job04(int qtdBlocos, long long tamVetor, long long tamBloco, int qtdLoop, float *vetor) {

    long long i, j, k, tmp;

    // Tamanho bloco atual (variavel). Inicialmente eh igual ao "tamBloco"
    long long tamBlocoAtual = tamBloco;

    // Tamanho restante:          
    long long tamRestante = tamVetor - tamBlocoAtual;

    long long index = 0;
    int terminou = 0;
    while (!terminou) {
        if (tamRestante <= tamBlocoAtual) {
            tamBlocoAtual += tamRestante;
            tamRestante = 0;
            terminou = 1;
        }
        printf("Calculando (%d vezes) Bloco de tamanho %lld com index %lld ao %lld...\n", qtdLoop, tamBlocoAtual, index,
               index + tamBlocoAtual - 1);
        for (k = 0; k < qtdLoop; k++) {
            for (j = 1; j < tamBlocoAtual; j++) {
                tmp = rand() % 100 + 1;
                vetor[index + j - 1] = tmp;
                vetor[index + j] = vetor[index + j - 1] * VALOR_QUALQUER + pow(tmp, 2);
                //calcula(&vetor[index+j-1], &vetor[index+j]);
            }
        }
        index += tamBlocoAtual;
        tamBlocoAtual += tamBloco;
        tamRestante -= tamBlocoAtual;
    }

}


/*
 * 
 */
int main(int argc, char **argv) {

    if (argc < 5) {
        fprintf(stderr, "exec tamanho_em_MB qtdLoops qtdBlocos jobType[1..4]\n");
        fprintf(stderr, "Ex: exec 1024 60 4 1\n");
        exit(1);
    }
    long long memoriaUtilizada = atoll(argv[1]);
    long long tamVetor = memoriaUtilizada * 1024 * 1024 / sizeof(float);
    long qtdLoop = atoi(argv[2]);
    int qtdBlocos = atoi(argv[3]);
    int jobType = atoi(argv[4]);


    float *vetor;
    vetor = malloc(sizeof(float) * tamVetor);


    /*
     *  The  memset()  function  fills  the first n bytes of the
        memory area pointed to by s with the constant byte c.
     */
    //memset(vetor, 0, tamVetor*4);

    /*
     * The  bzero() function sets the first n bytes of the area
       starting at s to zero (bytes containing ’\0’).
     */

    //bzero(vetor, tamVetor*4);

    long long tamBloco = tamVetor / qtdBlocos;

    printf("Memoria a ser utilizada: %lld MB\n", memoriaUtilizada);
    printf("Tamanho float: %lu Bytes\n", sizeof(float));
    printf("Tamanho do Vetor: %lld elementos\n", tamVetor);
    printf("Quantidade de Blocos: %d\n", qtdBlocos);
    printf("Tamanho de cada Bloco: %lld elementos\n", tamBloco);
    printf("Loops: %ld vezes\n", qtdLoop);

    if (jobType == 1 || jobType == 2)
        job01(qtdBlocos, tamBloco, qtdLoop, vetor);
    else if (jobType == 3)
        job03(qtdBlocos, tamVetor, tamBloco, qtdLoop, vetor);
    else if (jobType == 4)
        job04(qtdBlocos, tamVetor, tamBloco, qtdLoop, vetor);
    else
        printf("Error: jobType %d nao existe.\n", jobType);

    free(vetor);
    printf("Saindo...\n");
    return (EXIT_SUCCESS);
}
