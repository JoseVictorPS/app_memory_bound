/* 
 * File:   main.c
 * Author: rkendy
 *
 * Created on 5 de Junho de 2013, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


/*
 * 
 */
int main(int argc, char **argv) {

    if (argc < 4) {
        fprintf(stderr, "exec tamanho_em_MB qtdLoops qtdBlocos\n");
        fprintf(stderr, "Ex: exec 1024 60 4\n");
        exit(1);
    }
    long long memoriaUtilizada = atoll(argv[1]);
    long long tamVetor = memoriaUtilizada * 1024 * 1024 / sizeof(float);
    long tamLoop = atoi(argv[2]);
    int qtdBlocos = atoi(argv[3]);

    printf("Memoria a ser utilizada: %lld MB\n", memoriaUtilizada);

    printf("Tamanho float: %lu Bytes\n", sizeof(float));

    float *vetor;
    printf("Alocando vetor...\n");
    vetor = malloc(sizeof(float) * tamVetor);
    long long i, j, k, tmp;
    float valorqq = 10;

    /*
     *  The  memset()  function  fills  the first n bytes of the
        memory area pointed to by s with the constant byte c.
     */
    //memset(vetor, 0, tamVetor*4);

    /*
     * The  bzero() function sets the first n bytes of the area
       starting at s to zero (bytes containing ’\0’).
     */

    //bzero(vetor, tamVetor*4);

    long index = 0;
    long tamBloco = 0;
    long tamBlocoBase = tamVetor / qtdBlocos;
    int qtdBlocosProcessados = 0;

    //for(i=0 ; qtdBlocosProcessados < qtdBlocos ; i++) {
    while (qtdBlocosProcessados < qtdBlocos) {

        index += tamBloco;            // Define index inicial baseado no tamanho do bloco anterior
        tamBloco += tamBlocoBase;        // Define tamanho do bloco atual

        printf("Quantidade de Blocos %d...\n", (tamBloco / tamBlocoBase));
        printf("Tamanho de cada Bloco %ld...\n", tamBloco);
        printf("Calculando Bloco com index %ld ao %ld...\n", index, index + tamBloco - 1);

        for (k = 0; k < tamLoop; k++) {
            for (j = 1; j < tamBloco; j++) {
                tmp = rand() % 100 + 1;
                vetor[index + j - 1] = tmp;
                vetor[index + j] = vetor[index + j - 1] * valorqq + pow(tmp, tmp);
                //vetor[j] = vetor[j-1];
            }
        }
        qtdBlocosProcessados += (tamBloco / tamBlocoBase);
    }

    free(vetor);
    printf("Saindo...\n");
    return (EXIT_SUCCESS);
}