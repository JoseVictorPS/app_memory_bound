/* 
 * File:   main.c
 * Author: rkendy
 *
 * Created on 5 de Junho de 2013, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/sysinfo.h>

#define VALOR_QUALQUER 10



unsigned long get_host_freemem() {
    struct sysinfo memInfo;
    sysinfo(&memInfo);
    return memInfo.freeram;
}


void job01(int qtdBlocos, long long tamBloco, int qtdLoop, float *vetor) {

    long long i, j, k, tmp;


    for (i = 0; i < qtdBlocos; i++) {
        long long index = i * tamBloco;
        printf("Calculando (%d vezes) Bloco %lld com index %lld ao %lld...\n", qtdLoop, i, index, index + tamBloco - 1);
        for (k = 0; k < qtdLoop; k++) {
            for (j = 1; j < tamBloco; j++) {
                tmp = rand() % 100 + 1;
                vetor[index + j - 1] = tmp;
                vetor[index + j] = vetor[index + j - 1] * VALOR_QUALQUER + pow(tmp, tmp);
            }
        }
    }
}


/*
 * 
 */
int main(int argc, char **argv) {

    if (argc < 5) {
        fprintf(stderr, "exec tamanho_em_MB qtdLoops qtdBlocos jobType[1..4]\n");
        fprintf(stderr, "Ex: exec 1024 60 4 1\n");
        exit(1);
    }
    long long memoriaUtilizada = atoll(argv[1]);
    long qtdLoop = atoi(argv[2]);
    int qtdBlocos = atoi(argv[3]);
    int jobType = atoi(argv[4]);


    int tam_float = sizeof(float);
    unsigned long freeram = get_host_freemem();
    unsigned long freeram_memset = freeram - 200 * 1024 * 1024;
    unsigned long tamVetor = freeram / tam_float;
    unsigned long indice_limite_memset = freeram_memset / tam_float;

    float *vetor;
    vetor = malloc(tam_float * tamVetor);

    printf("free memory %ldMB\n", freeram / 1024 / 1024);
    printf("free memory memset %ldMB\n", freeram_memset / 1024 / 1024);

    printf("tamanho do vetor %ld\n", tamVetor);
    printf("tamanho do subvetor %ld\n", indice_limite_memset);

    memset(vetor, 0, indice_limite_memset * tam_float);

    long i, tmp;
    for (i = indice_limite_memset; i < tamVetor; i++) {
        tmp = rand() % 100 + 1;
        vetor[i] = pow(tmp, tmp) * pow(tmp, tmp);
    }
    printf("Indice final de i: %ld\n", i);

    free(vetor);
    printf("Saindo...\n");
    return (EXIT_SUCCESS);
}